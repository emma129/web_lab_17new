<%@ page import="java.io.File" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: yzhb363
  Date: 29/05/2017
  Time: 5:26 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>ex06</title>
</head>
<body>


<%--out.println("<table>");
    out.println("<tr><th>Thumbnail</th>");
        out.println("<th><a href='/ImageGalleryDisplay?sortColumn=filename&order="+ filenameSortToggle
				+ "ending'>Filename <img src='/images/sort-" + currFilenameSortToggle + ".png' alt='icon' /></a></th>");
        out.println("<th><a href='/ImageGalleryDisplay?sortColumn=filesize&order="+ filesizeSortToggle
				+ "ending'>File-size <img src='/images/sort-" + currFilesizeSortToggle + ".png'  alt='icon' /></a></th>");--%>


    <table>
        <tr>
            <th>Thumbnail</th>
            <th><a href="/ImageGalleryDisplay?sortColumn=filename&order=${fileNameSort}ending">Filename
                <img src="${fileNameIcon}" alt="icon"/></a>
            </th>
            <th><a href="${fileSizeSort}">File-size
                <img src="${fileSizeIcon}" alt='icon'/></a></th>
        </tr>
        <c:forEach var="file" items="${servletPhotoPath}">
        <tr>
            <td>
                <a href="Photos/${file.fullFile.getName()}">
                    <img src="Photos/${file.thumbPath.getName()}" alt="${file.thumbDisplay}"/>
                </a>
            </td>
            <td>
                    ${file.thumbDisplay}
            </td>
            <td>
                    ${file.fullfileSize}
            </td>
        </tr>
        </c:forEach>
    </table>



</body>
</html>
